using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CountPoints : MonoBehaviour
{
    [SerializeField] private SpawnAgro _spawnAgro;
    [SerializeField] private UseBaf _useBaf;

    private Text _countPoint;
    public int Count;
    private void Start()
    {
        _spawnAgro = _spawnAgro.GetComponent<SpawnAgro>();
        _countPoint = gameObject.GetComponent<Text>();
    }
    private void OnEnable()
    {
        DeathAgro.ClearSpawnPoint += CountPointsText;
    }
    private void OnDisable()
    {
        DeathAgro.ClearSpawnPoint -= CountPointsText;
    }
    private void CountPointsText(Transform temp)
    {
        Count++;
        _countPoint.text =Count.ToString();

        //Increasing complexity
        //���� � ���������� �� ������������ 
        if ((Count % 10) == 0)
        {
            _spawnAgro.BustHpEmpty += 1;
            _spawnAgro.TimingSpanw -= .25f;
            Baf();
        }
    }

    private void Baf()
    {
        if (Random.Range(0,2) ==0)
        {
            _useBaf.AllDethButton();
        }
        else
        {
            _useBaf.Frizbutton();
        }
    }
}

