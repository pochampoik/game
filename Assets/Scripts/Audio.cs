using UnityEngine;

public class Audio : MonoBehaviour
{
    [SerializeField] private AudioSource _DeathSound;
    [SerializeField] private AudioSource _SoundTap;
    private void OnEnable()
    {
        DeathAgro.ClearSpawnPoint += DeathSound;
    }
    private void OnDisable()
    {
        DeathAgro.ClearSpawnPoint -= DeathSound;
    }

    private void DeathSound(Transform temp)
    {
        _DeathSound.Play();
        
    }

    private void Update()
    {
        if (Input.GetMouseButton(0))
        {
            _SoundTap.Play();
        }
    }
}
