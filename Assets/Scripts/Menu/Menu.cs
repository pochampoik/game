using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
   public void MenuScenes(int numberScenes)
    {
        SceneManager.LoadScene(numberScenes);
    }
    public void MenuExit()
    {
        Application.Quit();
    }
}


