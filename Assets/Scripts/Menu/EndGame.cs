using UnityEngine;

public class EndGame : MonoBehaviour
{
    [SerializeField] private GameObject _panelGameOver;
    [SerializeField] private CountPoints _count;
    [SerializeField] private SpawnAgro _spawnAgro;

    public delegate void TrigGemaOver(int _countPoints);
    public static event TrigGemaOver CountPoinsInRecords;

    private void Start()
    {
        gameObject.SetActive(false);
        _count.GetComponent<CountPoints>();
    }

    public void GameOver()
    {
        _panelGameOver.SetActive(true);
        CountPoinsInRecords(_count.Count);
        _spawnAgro.IsSpawn = false;
    }
}
