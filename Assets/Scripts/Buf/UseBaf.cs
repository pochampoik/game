using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UseBaf : MonoBehaviour
{
    [SerializeField] private GameObject _frizButton;
    [SerializeField] private GameObject _allDeathButton;
    [SerializeField] private SpawnAgro _spawnAgro;

    [SerializeField] private AudioSource _soundBobm;
    [SerializeField] private AudioSource _soundFriz;

    private int _frizTime = 3;
    public void AllDethButton()
    {
        _allDeathButton.SetActive(true);
    }
    public void Frizbutton()
    {
        _frizButton.SetActive(true);
    }

    public void UsengBaf(int _number)
    {
        if (_number ==1)
        {
            _allDeathButton.SetActive(false);
            GameObject[] objectsForLook = GameObject.FindGameObjectsWithTag("Mob");
            for (int i = 0; i < objectsForLook.Length; i++)
            {
                objectsForLook[i].GetComponent<Helfs>().BafDeathAgro();
            }
            _soundBobm.Play();
        }
        else
        {
            _soundFriz.Play();
            _frizButton.SetActive(false);
            _spawnAgro = _spawnAgro.GetComponent<SpawnAgro>();
            _spawnAgro.BafFrize = _frizTime;
            
        }
    } 
    
}
