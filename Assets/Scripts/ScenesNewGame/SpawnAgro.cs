using System.Collections.Generic;
using UnityEngine;

public class SpawnAgro : MonoBehaviour
{
    [SerializeField] private Transform[] _spawnsPointAgro;
    [SerializeField] private GameObject _prefabAgro;
    [SerializeField] private EndGame _endGame;

    public bool IsSpawn = true;

    private List<Transform> _tempSpawnPointAgro = new List<Transform>();
    public float TimingSpanw = 3f;
    private float _time = 0;
    private int _emptyCounter;

    public int BafFrize = 0;
    public int BustHpEmpty = 0;


    private void Start()
    {
        foreach (var item in _spawnsPointAgro)
        {
            _tempSpawnPointAgro.Add(item);
        }
    }

    private void Update()
    {
        _time += Time.deltaTime;
        if ((_time >= TimingSpanw+ BafFrize) )
        {
            _time = 0;
            BafFrize = 0;
            SpawnerAgro(IsSpawn);
        }
    }

    private void SpawnerAgro(bool isSpanw)
    {
        int _CountListSpawns = _tempSpawnPointAgro.Count - 1;
        if (_tempSpawnPointAgro.Count > 0 && isSpanw)
        {
            _emptyCounter++;
            int _numberSpawnPoint = Random.Range(0, _CountListSpawns);
            var temp =Instantiate(_prefabAgro, _tempSpawnPointAgro[_numberSpawnPoint]);
            temp.GetComponent<Agro>().SpawnPointThisAgro = _tempSpawnPointAgro[_numberSpawnPoint];
            temp.GetComponent<Helfs>().IncreasingComplexityHp += BustHpEmpty;
            _tempSpawnPointAgro.Remove(_tempSpawnPointAgro[_numberSpawnPoint]);
        }
        if (_emptyCounter >= 10)
        {
            _endGame.GetComponent<EndGame>().GameOver();
        }
    }

    private void OnEnable()
    {
        DeathAgro.ClearSpawnPoint += AddClearSpawnPoint;
    }
    private void OnDisable()
    {
        DeathAgro.ClearSpawnPoint -= AddClearSpawnPoint;
    }
    private void AddClearSpawnPoint(Transform _spawnPoint)
    {
        _emptyCounter--;
        _tempSpawnPointAgro.Add(_spawnPoint);
    }
}
