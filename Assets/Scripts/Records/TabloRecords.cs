using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using TMPro;

public class TabloRecords : MonoBehaviour
{
    [SerializeField] private TMP_Text _textRecords;
    public Item item;
    private string path;


    private void Start()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
        path =Application.persistentDataPath +"/Records.json";
#else
       path = Application.streamingAssetsPath + "/Records.json";
#endif
        if (!File.Exists(path))
        {
            CreateRecordsJson();
        }
        LoadField();
        WritingRecords();
    }

    private void LoadField()
    {
        item = JsonUtility.FromJson<Item>(File.ReadAllText(path));
    }
    
    private void WritingRecords()
    {
        for (int i = 0; i < item.RecodsCount.Count ; i++)
        {
            _textRecords.text += (i +1)+":" + item.RecodsCount[i]+"\n" ;
        }
    }
    private void CreateRecordsJson()
    {
        for (int i = 0; i < 10; i++)
        {
            item.RecodsCount.Add(0);
        }
        File.WriteAllText(path, JsonUtility.ToJson(item));
    }
}


[System.Serializable]
public class Item
{
    public List<int> RecodsCount;
}

