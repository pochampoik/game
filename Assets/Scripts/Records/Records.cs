using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class Records : MonoBehaviour
{
    public Item item;
    private string path;
    private void Start()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
        path =Application.persistentDataPath +"/Records.json";
#else
        path = Application.streamingAssetsPath + "/Records.json";
#endif
        LoadField();
    }
    private void LoadField()
    {
        item = JsonUtility.FromJson<Item>(File.ReadAllText(path));
    }
    private void SaveField()
    {
        File.WriteAllText(path, JsonUtility.ToJson(item));
    }

    private void BubbleSort(List<int> bubbls)
    {
        int temp;
        for (int i = 0; i < bubbls.Count; i++)
        {
            for (int j = 0; j < bubbls.Count ; j++)
            {
                if(bubbls[i]> bubbls[j])
                {
                    
                    temp = bubbls[i];
                    bubbls[i] = bubbls[j];
                    bubbls[j] = temp;
                }
            }
        }
        for (int i = 0; i < bubbls.Count; i++)
        {
            item.RecodsCount[i] = bubbls[i];
            
        }
    }

    private void OnEnable()
    {
        EndGame.CountPoinsInRecords += AddNewRecords;
    }
    private void OnDisable()
    {
        EndGame.CountPoinsInRecords -= AddNewRecords;
    }

    private void AddNewRecords(int _newRecords)
    {
        if (_newRecords >item.RecodsCount[item.RecodsCount.Count-1])
        {
            item.RecodsCount[item.RecodsCount.Count-1] = _newRecords;
            BubbleSort(item.RecodsCount);
            SaveField();
        }
    }
}