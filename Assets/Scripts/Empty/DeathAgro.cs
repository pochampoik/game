using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathAgro : MonoBehaviour
{

    [SerializeField] private Agro _empty;
    [SerializeField] private AudioSource _deathSound;

    public delegate void OntriggerDeath(Transform spawnPoint);
    public static event OntriggerDeath ClearSpawnPoint;

    public void Death()
    {
        ClearSpawnPoint(_empty.SpawnPointThisAgro);
        _deathSound.Play();
        Destroy(gameObject);
        
    }

}
