using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Helfs : MonoBehaviour
{
    [SerializeField] private int _helfs;
    [SerializeField] GameObject _hpBarPrefab;
    [SerializeField] Transform _hpBarAncor;
    [SerializeField] DeathAgro _death;

    private Image _helfBarStripOfLives;
    private Text _barTextHp;
    private int _tuchDamage = 1;
    private int _maxhelfs;


    public int IncreasingComplexityHp = 0 ;
    private void Awake()
    {
        var _bar = Instantiate(_hpBarPrefab, _hpBarAncor);
        var temp=_bar.GetComponent<TextHp>();
        _death.GetComponent<DeathAgro>();
        _barTextHp = temp.TexthpBar;
        _helfBarStripOfLives = temp.StripOfLives;
    }

    private void Start()
    {
        RandomHelfAgro(IncreasingComplexityHp);
    }

    private void RandomHelfAgro(int _hpBuf)
    {
        _helfs = Random.Range(1 + _hpBuf, 3 + _hpBuf);
        _maxhelfs = _helfs;
        _barTextHp.text = _helfs + " / " + _maxhelfs;
    }

    public void SetDamage()
    {
        _helfs -= _tuchDamage;
        _helfBarStripOfLives.fillAmount = ((_helfs * 100) / _maxhelfs) / 100f;
        _barTextHp.text = _helfs + " / " + _maxhelfs;

        if (_helfs <=0)
        {
            _death.Death();
        }
    }

    public void BafDeathAgro()
    {
        _death.Death();
    }
}
