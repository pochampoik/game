using UnityEngine;
using UnityEngine.EventSystems;

public class Agro : MonoBehaviour, IPointerClickHandler
{ 
    public Transform SpawnPointThisAgro;
    private Helfs _hp;

    private void Awake()
    
    { 
        _hp = GetComponent<Helfs>();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        _hp.SetDamage();
    } 
}